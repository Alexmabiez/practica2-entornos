package Libreria;

import java.util.Scanner;

public class Class1 {
	static Scanner in = new Scanner(System.in);
	
	public static int getNum(String g) {
		int i=0;
		System.out.print(g);
		do{
			if(in.hasNextInt()){
				i=in.nextInt();
				in.nextLine();
			}else{
				in.nextLine();
				System.out.println("Error.");
			}
		
		}while(i==0);
		return i;
	}

	public static void palabras() {
		System.out.println("Introduce una frase...");
		String g = in.nextLine();
		String c[]=g.split(" ");
		System.out.println("Hay "+c.length+" palabras.");
		
	}

	public static void vocales() {
		System.out.println("Introduce una frase...");
		String g = in.nextLine();
		String c[]=g.split("");
		int t = 0;
		for (int i = 0; i < c.length; i++) {
			if(c[i].equals("a") ||c[i].equals("e")||c[i].equals("i")||c[i].equals("o")||c[i].equals("u")){
				t++;
			}
		}
		System.out.println("Hay "+t+" vocales.");
		
		
	}

	public static void letras() {
		System.out.println("Introduce una frase...");
		String g = in.nextLine();
		System.out.println("Hay "+g.length()+" caracteres.");
		
	}
	
	public static void Enter()
    {
        System.out.print("\nPresiona Enter para continuar...");
        in.hasNextLine();
    }
	
	 
}
