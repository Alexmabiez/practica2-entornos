﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_alejandro
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }
   
        public static void Menu()
        {
            Console.WriteLine("Seleccione una opcion:");
            Console.WriteLine("1.\tContar caracteres");
            Console.WriteLine("2.\tContar vocales");
            Console.WriteLine("3.\tContar palabras");
            Console.WriteLine("4.\tSalir");
            int i = 0;

            do
            {
                i = GetNum("");
                if (i < 1 || i > 4)
                {
                    i = 0;
                    Console.WriteLine("Por favor selecione una opcion valida.");
                }
            } while (i == 0);

            switch (i)
            {
                case 1:
                    Letras();
                    break;
                case 2:
                    Vocales();
                    break;
                case 3:
                    Palabras();
                    break;
                case 4:
                    Salir();
                    break;

            }
            Console.WriteLine("Presiona Enter para continuar...");
            Console.ReadKey();
            Menu();
        }
        private static void Palabras()
        {
            Console.WriteLine("Introduce una frase...");
            String g = Console.ReadLine();
            String[] c = g.Split(' ');
            Console.WriteLine("Hay " + c.Length + " palabras.");

        }

        private static void Vocales()
        {
            Console.WriteLine("Introduce una frase...");
            String g = Console.ReadLine();
            int t = 0;
            foreach (char c in g)
            {
                    if (c=='a' || c == 'e' || c == 'o' || c == 'i' || c == 'u')
                {
                    t++;
                }
            }
            Console.WriteLine("Hay " + t + " vocales.");


        }

        private static void Letras()
        {
            Console.WriteLine("Introduce una frase...");
            String g = Console.ReadLine();
            Console.WriteLine("Hay " + g.Length + " caracteres.");

        }
        public static void Salir()
        {
            Environment.Exit(0);
        }


        public static int GetNum(String g)
        {
            int i = 0;
            Console.Write(g);
            do
            {
                String s = Console.ReadLine();
                try
                {
                    i = Int32.Parse(s);
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            } while (i == 0);
            return i;
        }

    }
}
