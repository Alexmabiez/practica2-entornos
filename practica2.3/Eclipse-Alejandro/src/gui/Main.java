package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JList;
import javax.swing.JRadioButton;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.List;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
/*
 * @author Alejandro Martin
 * 
 * @since 30/01/2018
 */
public class Main extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Editar");
		menuBar.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JList list = new JList();
		list.setBounds(5, 5, 0, 227);
		contentPane.add(list);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.YELLOW);
		panel.setBounds(5, 5, 419, 227);
		contentPane.add(panel);
		panel.setLayout(null);
		
		Label label = new Label("Nombre ");
		label.setBounds(10, 10, 62, 22);
		panel.add(label);
		
		TextArea textArea = new TextArea();
		textArea.setBounds(253, 10, 156, 133);
		panel.add(textArea);
		
		TextField textField = new TextField();
		textField.setBounds(76, 10, 156, 22);
		panel.add(textField);
		
		Button button = new Button("Enter");
		button.setBounds(10, 52, 70, 22);
		panel.add(button);
		
		Checkbox checkbox = new Checkbox(">18");
		checkbox.setBounds(10, 84, 95, 22);
		panel.add(checkbox);
		
		List list_1 = new List();
		list_1.setMultipleSelections(false);
		list_1.setBounds(10, 112, 110, 60);
		panel.add(list_1);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Denegada");
		rdbtnNewRadioButton.setBounds(111, 83, 109, 23);
		panel.add(rdbtnNewRadioButton);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Main.class.getResource("/gui/mikkeller-logo.png")));
		lblNewLabel.setBounds(126, 149, 283, 67);
		panel.add(lblNewLabel);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"IPA", "APA", "PORTER"}));
		comboBox.setBounds(10, 182, 110, 20);
		panel.add(comboBox);
	}
}
