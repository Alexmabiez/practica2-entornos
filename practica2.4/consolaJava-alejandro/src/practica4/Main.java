package practica4;

import java.util.Scanner;

public class Main {
	static Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		menu();
	}
	
	public static void menu(){
		System.out.println("Seleccione una opcion:");
		System.out.println("1.\tContar caracteres");
		System.out.println("2.\tContar vocales");
		System.out.println("3.\tContar palabras");
		System.out.println("4.\tSalir");
		int i=0;
		
		do{
			i=getNum("");
			if(i<1||i>4){
				i=0;
				System.out.println("Error. Opcion no valida.");
			}
		}while(i==0);
			
		switch(i){
		case 1:
			letras();
			break;
		case 2:
			vocales();
			break;
		case 3:
			palabras();
			break ;
		case 4:
			salir();
			break;
			
		}
		System.out.println("Pulse Enter si desea continuar...");
		in.hasNextLine();
		menu();
	}
	private static int getNum(String g) {
		int i=0;
		System.out.print(g);
		do{
			if(in.hasNextInt()){
				i=in.nextInt();
				in.nextLine();
			}else{
				in.nextLine();
				System.out.println("Error.");
			}
		
		}while(i==0);
		return i;
	}

	private static void palabras() {
		System.out.println("Introduce una frase...");
		String g = in.nextLine();
		String c[]=g.split(" ");
		System.out.println("Hay "+c.length+" palabras.");
		
	}

	private static void vocales() {
		System.out.println("Introduce una frase...");
		String g = in.nextLine();
		String c[]=g.split("");
		int t = 0;
		for (int i = 0; i < c.length; i++) {
			if(c[i].equals("a") ||c[i].equals("e")||c[i].equals("i")||c[i].equals("o")||c[i].equals("u")){
				t++;
			}
		}
		System.out.println("Hay "+t+" vocales.");
		
		
	}

	private static void letras() {
		System.out.println("Introduce una frase...");
		String g = in.nextLine();
		System.out.println("Hay "+g.length()+" caracteres.");
		
	}

	public static void salir(){
		System.exit(0);
	}
}
