﻿using System;

namespace LibreriaVS_AlejandroMartin
{
    public class Class1
    {
        public static int LeerEnteroPositivo(){
            int i = -1;
            do
            {
                String str = Console.ReadLine();
                try
                {
                    i = Int32.Parse(str);
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            } while (i < 0);
            return i;

        }

        public static int LeerEnteroNegativo()
        {
            int i = 1;
            do
            {
                String str = Console.ReadLine();
                try
                {
                    i = Int32.Parse(str);
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            } while (i > 0);
            return i;

        }

        public static void EsPrimo(int n)
        {
            int a = 0, i;
            for (i = 1; i < (n + 1); i++)
            {
                if (n % i == 0)
                {
                    a++;
                }
            }
            if (a > 2)
            {
                Console.WriteLine(n + " No es primo");
            }
            else
            {
                Console.WriteLine(n + " Es primo");
            }

        }

        public static void Potencia(int n, int u) {
            Console.WriteLine("la potencia de "+n+" al "+u+" es "+Math.Pow(n,u));
        }

        public static void MostrarMenu() {

            Console.WriteLine("Seleccione una opcion:");
            Console.WriteLine("1.\tContar caracteres");
            Console.WriteLine("2.\tContar vocales");
            Console.WriteLine("3.\tContar palabras");
            Console.WriteLine("4.\tSalir");
        }
    }
}
