﻿using System;

namespace LibreriaVS_AlejandroMartin
{
    public class Class2
    {
        public static void Enter()
        {
            Console.Write("\nPresiona Enter para continuar...");
            Console.ReadKey();
        }

        public static void EsPerfecto(int numero)
        {
            int suma = 0;
            for (int i = 1; i < numero; i++)
            {
                if (numero % i == 0)
                {
                    suma = suma + i;
                }

            }


            if (suma == numero)
            {
                Console.WriteLine("El numero es perfecto");
            }
            else
            {
                Console.WriteLine("El numero no es perfecto");
            }
        }

        public static void Multplicacion(int x, int y) {
            Console.WriteLine("el resultado es: "+(x*y));
        }

        public static void Resta(int x, int y)
        {
            Console.WriteLine("el resultado es: " + (x - y));
        }

        public static void Suma(int x, int y)
        {
            Console.WriteLine("el resultado es: " + (x + y));
        }
    }
}
